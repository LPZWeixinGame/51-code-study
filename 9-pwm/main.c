#include <reg52.h>
#include "types.h"

sbit PWM_OUT = P0^0;

// 视频的第十课，1:04:32
// 视频的第十课，1:34:02 中有个计算器+长短按键的项目，挺好的，可以写下

static void led_7_turn_on_off();

// Start interrupt setting

void Init_Timer0_Interrupt() {
	// start timer0
	TR0 = 1;
	TMOD |= 0x01;
	
	// start timer0 interrupt
	EA = 1;
	ET0 = 1;
	TR0 = 1;
	
	TL0 = 0x18;
	TH0 = 0xFC;
}


void Timer0_Interrupt_Run() interrupt 1 {
	TL0 = 0x18;
	TH0 = 0xFC;

	led_7_turn_on_off();
}

sbit LED_7 = P2^7;
static void led_7_turn_on_off() {
	static u16 led_7_count = 0;
	led_7_count++;
	if (led_7_count >= 1000) {
		LED_7 = !LED_7;
		led_7_count = 0;
	}
}

void main() {
	Init_Timer0_Interrupt();
	
	while(1) {
	}
}
