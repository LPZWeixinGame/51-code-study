#include <reg52.h>
#include "types.h"
#include "uart.h"

static u8 T0RH = 0;
static u8 T0RL = 0;

static void led_7_turn_on_off();
static void _Check_Key1();

// Start interrupt setting

static u16 Timer1_Config_Delay(u16 ms) {
    u32 tmp;
    
    tmp = 11059200/12;  // 定时器计数频率
    tmp = (tmp * ms) / 1000;  // 计算所需要的计数
    tmp = 65536 - tmp;   // 计算定时器重载值
    tmp = tmp + 18;      // 补偿中断响应延时造成的误差
    
    return tmp;
}

void Timer0_Init_Interrupt() {
    u16 T0TH = Timer1_Config_Delay(1);
    
	// start timer0
	TMOD &= 0xF0;
    TMOD |= 0x01;
	
    T0RH = (u8)(T0TH >> 8);  // 加载T0 的重载值
    T0RL = (u8)(T0TH);
	TH0 = T0RH;
	TL0 = T0RL;

	//TL0 = 0x18;
	//TH0 = 0xFC;
    
    // start timer0 interrupt
	EA = 1;
	ET0 = 1;
    TR0 = 1;
}


void Timer0_Interrupt_Run() interrupt 1 {
	TH0 = T0RH;
	TL0 = T0RL;
	//TL0 = 0x18;
	//TH0 = 0xFC;

	led_7_turn_on_off();
    
    _Check_Key1();
}

sbit LED_7 = P2^7;
static void led_7_turn_on_off() {
	static u16 led_7_count = 0;
	led_7_count++;
	if (led_7_count >= 1000) {
		LED_7 = !LED_7;
		led_7_count = 0;
	}
}

/// ------------------------------------

static void Timer1_Init_Interrupt() {

    Uart_Init();
    
    // 如果使用定时器2，最好使用模式2
    TMOD &= 0x0f; // 清零T1 控制位
    TMOD |= 0x20; // 配置T1 的模式为2
    
    // 使用码率 9600
    TH1 = Uart_Config_Band(9600);
    TL1 = TH1;
    
    ES = 1;
    EA = 1;  // 使能总中断设置为1
    TR1 = 1; // 最后设置定时器，是在保证上面设置参数后才打开定时器，防止定时器打开了还没有设置参数

    /*
	TMOD|=0X20;	//设置计数器工作方式2
	SCON=0X50;	//设置为工作方式1
	PCON=0X80;	//波特率加倍
	TH1=0XFA;	//计数器初始值设置
	TL1=TH1;
	ES=1;		//打开接收中断
	EA=1;		//打开总中断
	TR1=1;		//打开计数器	
    */
}

sbit LED_6 = P2^6;
sbit LED_5 = P2^5;
sbit LED_4 = P2^4;
sbit LED_1 = P2^1;

static u8 _Receive_Buf = 0;

// 如果输入的1，6号灯开关；如果输入其他的，5号灯开关
static void _Change_LED() {
    if (_Receive_Buf == 6) {
        LED_6 = !LED_6;
    }
    else if (_Receive_Buf == 5) {
        LED_5 = !LED_5;
    }
    else {
        LED_4 = !LED_4;
    }
}

void Timer1_Interrupt_Run() interrupt 4 {
    if (Uart_Read_Buf(&_Receive_Buf)) {
        _Change_LED();
        Uart_Write_Buf(_Receive_Buf);
    }
    
    /* for test
    if (RI) {
        RI = 0; // 手动清零接收中断标志位
        _Receive_Buf = SBUF;  // 接收到数据并保存到接收字节中
        SBUF = _Receive_Buf+1;  // 接收到的数据又直接发回，又叫"echo"，用以提示用户输入的信息是否已经正确接收
        _Change_LED();
    }
    
    if (TI) {   // 字节发送完毕
        TI = 0; // 手动清零发送中断标志位
    }
    */
}

/// ------------------------------------


/// ------------------------------------
// 添加一个按钮检测，如果有按键按下，发送数据给PC端

sbit _Button_Key1 = P3^1;
static u8 _Check_FLAG_Key1 = 0;
static BOOL _Flag_Key1_Press = 0;

static void _On_Key1_Press() {
    _Flag_Key1_Press = TRUE;
}

static void _On_Key1_Release() {
    if (_Flag_Key1_Press == TRUE) {
        _Flag_Key1_Press = FALSE;
        
        // 发送数据
        //TI = 0;
        //SBUF = 3;
        Uart_Write_Buf(10);
        
        LED_1 = !LED_1;
    }
}

static void _Check_Key1() {
    _Check_FLAG_Key1 = (_Check_FLAG_Key1 << 1) | _Button_Key1;
    if (_Check_FLAG_Key1 == 0xFF) {
        // 释放
        _On_Key1_Release();
    }
    else if (_Check_FLAG_Key1 == 0x00) {
        // 按下
        _On_Key1_Press();
    }
}
/// ------------------------------------


void main() {
	Timer0_Init_Interrupt();
    Timer1_Init_Interrupt();
	
	while(1) {
        // 做频繁闪烁和轮询的动作
	}
}
