#include "uart.h"
#include <reg52.h>

void Uart_Init() {
    // 打开定时器1 和 中断1
    // TMOD |= 0x02;  // 定时器最好是在一个地方设置，外部已经设置
    
    SCON |= 0x50;
    PCON |= 0X80;
}

BOOL Uart_Check_Read_Begin() {
    return RI == 1;
}

BOOL Uart_Check_Write_End() {
    return TI == 1;
}

BOOL Uart_Read_Buf(u8* receBuf) {
    if (RI) {
        *receBuf = 0;
        RI = 0; // 手动清零接收中断标志位
        *receBuf = SBUF; // 接收到数据并保存到接收字节中
        return TRUE;
    }
    return FALSE;
}

BOOL Uart_Write_Buf(u8 sendBuf) {
    SBUF = sendBuf;
    if (TI) {
        TI = 0;
        return TRUE;
    }
    return FALSE;
}

u16 Uart_Config_Band(u16 band) {
    //return 0XFA; // 11059200 频率的波特率， 0xFA = 250
    return 256 - (11059200/12/16)/band; // 计算方式
}
