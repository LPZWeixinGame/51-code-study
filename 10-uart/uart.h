#ifndef _UART_H_
#define _UART_H_

#include "types.h"

// uart 串口通信，在使用工具测试时，有出现发送和接收不全的情况，暂时没有精力去查看情况，后续有时间需要搜索下
void Uart_Init();

BOOL Uart_Check_Read_Begin();

BOOL Uart_Check_Write_End();

BOOL Uart_Read_Buf(u8* receBuf);

BOOL Uart_Write_Buf(u8 sendBuf);

u16 Uart_Config_Band(u16 band);

#endif