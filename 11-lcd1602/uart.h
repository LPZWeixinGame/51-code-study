#ifndef _UART_H_
#define _UART_H_

#include "types.h"

// uart 串口通信，在使用工具测试时，有出现发送和接收不全的情况，暂时没有精力去查看情况，后续有时间需要搜索下
void Uart_Init();

BOOL Uart_Read_Buf(u8* receBuf);

// 暂时没有用到
u16 Uart_ReadBuf(u8* buf, u16 readLen);

void Uart_WriteBuf(u8* buf, u16 writeLen);

// 放在定时器1ms 中断处
// 暂时没有用到
void Uart_ReadMonitor(u8 ms);

// 放在uart 消息中断处
void Uart_WriteMonitor();

u16 Uart_ConfigBand(u16 band);

#endif