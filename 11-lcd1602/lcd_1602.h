#ifndef _LCD1602_H_
#define _LCD1602_H_

#include "types.h"

void LCD1602_Init();

BOOL LCD1602_ShowData(u8 x, u8 y, u8* msg);

void LCD1602_Clear();

void LCD1602_ShowChar(u8 x, u8 y, u8 msg);

void LCD1602_ShowNum(u8 x, u8 y, i32 num, u8 showLenNum);

void LCD1602_ShowBinary(u8 x, u8 y, u8 num);

#endif