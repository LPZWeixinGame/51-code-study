#ifndef _KEY_BOARD_H_
#define _KEY_BOARD_H_

#include "types.h"

typedef (*KeyBoardOnClickPtr)(u16 x, u16 y);

void KeyBoard_Init(KeyBoardOnClickPtr on_click_ptr);

void KeyBoard_Dirver();

void KeyBoard_Scan();

#endif