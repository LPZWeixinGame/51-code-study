#include <reg52.h>
#include <stdlib.h>
#include "types.h"
#include "timer.h"
#include "static_led.h"
#include "hc_74_138.h"

#define DYNAMIC_LED_LENGTH 8

sbit LED_1 = P2^1;
sbit LED_7 = P2^7;

u32 sec = 12345678;
u8 dynamic_led_index = 0;

u8 LedBuffer[DYNAMIC_LED_LENGTH] = {0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff};

// timer1 = (Timer*)malloc(sizeof(Timer)*2); timer1 is NULL. malloc is not useful.

Timer timer[2];


// dynamic led
// use 74HC138 and 74HC245

/*
// reflush the dynamic led per 1 microsecond
void Dynamic_LED_1_TimeOut() {
	// reflush
	P0 = 0xff; // changing the 138 from 1 to 2, there is some temp state to cause some shadow
	Change_HC_74_138(dynamic_led_index+1);
	P0 = LedBuffer[dynamic_led_index];
	
	dynamic_led_index++;
	if (dynamic_led_index >= DYNAMIC_LED_LENGTH) {
		dynamic_led_index = 0;
	}
}
*/

/*
// the stack may be full. need to use printf the log
// reflush the led number per 1 second
u8 shift = 0;
void Dynamic_LED_1000_TimeOut() {
	u8 index = 0;
	u8 shift1;

	LED_7 = !LED_7;
	shift++;

	for (; index < DYNAMIC_LED_LENGTH; index++) {
		shift1 = shift + index;
		LedBuffer[index] = static_led[shift1];
	}
	
	if (shift >= 8) {
		shift = 0;
	}
}
*/
void Dynamic_LED_1000_TimeOut() {
	LED_7 = !LED_7;

	sec++;
	LedBuffer[0] = static_led[sec/1%10];
	LedBuffer[1] = static_led[sec/10%10];
	LedBuffer[2] = static_led[sec/100%10];
	LedBuffer[3] = static_led[sec/1000%10];
	LedBuffer[4] = static_led[sec/10000%10];
	LedBuffer[5] = static_led[sec/100000%10];
	LedBuffer[6] = static_led[sec/1000000%10];
	LedBuffer[7] = static_led[sec/10000000%10];
	
	/*
	The stack is small in MCU C52. Do not use 'for' cycle. 
	u8 index = 0;
	u32 shift;
	sec++;
	LED_7 = !LED_7;
	for (shift = 1; index < DYNAMIC_LED_LENGTH; index++, shift*=10) {
		LedBuffer[index] = static_led[sec/shift%10];
	}
	*/
}




/*
void Dynamic_LED_1000_TimeOut() {
	u8 index = 0;
	//u8 tmp[DYNAMIC_LED_LENGTH] = {0};
	u32 tmp=1;
	u32 shift=1;
	
	sec++;
	LED_7 = !LED_7;
	
	tmp = shift+1; // add one is error. I think the stack is full
	
	for (shift = 1; index < DYNAMIC_LED_LENGTH; index++, shift*=10) {
		//LedBuffer[index] = static_led[sec/shift%10];
		//tmp[index] = (u8)(sec/shift%10);
		tmp = 10/shift;
		tmp = tmp %10;
	}
	shift = 10;
}
*/

void Dynamic_LED_TimerOutError(u8 err) {
	err = 1;
	//LED_7 = 1;
}

void TimerInterrupt0() {
	// reflush
	P0 = 0xff; // changing the 138 from 1 to 2, there is some temp state to cause some shadow
	Change_HC_74_138(dynamic_led_index+1);
	P0 = LedBuffer[dynamic_led_index];
	
	dynamic_led_index++;
	if (dynamic_led_index >= DYNAMIC_LED_LENGTH) {
		dynamic_led_index = 0;
	}
}

void TimerInterrupt1() {
}

void Init_Dynamic_Timer() {
	timer[0].dalay_ms = 1000;
	timer[0].count = 0;
	timer[0].timeOut = Dynamic_LED_1000_TimeOut;
}

void Init_Dynamic_Led() {
	u8 index = 0;
	u32 shift;
	for (shift = 1; index < DYNAMIC_LED_LENGTH; index++, shift*=10) {
		LedBuffer[index] = static_led[sec/shift%10];
	}
}

void Test_Dynamic() {
	Init_Dynamic_Timer();
	Init_Dynamic_Led();
	
	Init_HC_74_138();
	while (1) {
		Delay_Timer_Mode1(timer, 1, Dynamic_LED_TimerOutError);
	}
}

static void Init_Interrupt() {
	TimerInterrput timerInterrupt;
	timerInterrupt.t0_open_flag = TRUE;
	timerInterrupt.t1_open_flag = FALSE;
	timerInterrupt.timerInterrupt0 = TimerInterrupt0;
	timerInterrupt.timerInterrupt1 = TimerInterrupt1;
	Init_Timer_Interrupt(&timerInterrupt);
}

void main() {
	Init_Timer(TRUE, FALSE);
	Init_Interrupt();
	Test_Dynamic();
}
