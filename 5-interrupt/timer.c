#include <reg52.h>
#include <stdlib.h>
#include "types.h"
#include "timer.h"

#define SHORT_MAX 65535

static TimerInterrupt0Ptr _TimerInterrupt0;
static TimerInterrupt1Ptr _TimerInterrupt1;

static unsigned char delay_1_ms_flag0 = 0;
static unsigned char delay_1_ms_flag1 = 0;

void Init_Timer(BOOL t0_open, BOOL t1_open) {
	TR0 = t0_open;
	TR1 = t1_open;
	
	TMOD |= 0x01;
}

void Init_Timer_Interrupt(TimerInterrput* timerInterrupt) {
	if (timerInterrupt != NULL) {
		_TimerInterrupt0 = timerInterrupt->timerInterrupt0;
		_TimerInterrupt1 = timerInterrupt->timerInterrupt1;
		
		EA = 1;
		if (timerInterrupt->t0_open_flag) {
			ET0 = 1;
			TR0 = 1;
		}
		if (timerInterrupt->t1_open_flag) {
			ET1 = 1;
			TR1 = 1;
		}
		delay_1_ms_flag0 = 0;
		delay_1_ms_flag1 = 0;
	}
}

void timer0_interrupt() interrupt 1 {
	delay_1_ms_flag0 = 1;
	TL0 = 0x18;
	TH0 = 0xFC;
	if (_TimerInterrupt0) {
		_TimerInterrupt0();
	}
}

void timer1_interrupt() interrupt 3 {
	delay_1_ms_flag1 = 1;
	if (_TimerInterrupt1) {
		_TimerInterrupt1();
	}
}

void Delay_Timer_Mode1(Timer timer[], u8 timer_length, TimerOutErrorPtr errorPtr) {
	u8 i = 0;
	if (timer_length <= 0) {
		if (errorPtr != NULL) {
			errorPtr(-1);
		}
		return;
	}
	
	// 12MHz, 1ms = 1000 count, u16 = 65536, TH0+TL0 = 65536-1000 = 64536, 0xFC18, TH0 = 0xFC, TL0 = 0x18
	TL0 = 0x18;
	TH0 = 0xFC;

	while (1) {
		if (delay_1_ms_flag0 == 1) {
			delay_1_ms_flag0 = 0;
			for (i = 0; i < timer_length; i++) {
				timer[i].count++;
				if (timer[i].count >= timer[i].dalay_ms) {
					timer[i].timeOut();
					timer[i].count = 0;
				}
			}
		}
	}
}
