#ifndef _STATIC_LED_H_
#define _STATIC_LED_H_

#include "types.h"

#define STATIC_LED_MAX 17

// dp,g,f,e,d,c,b,a, from high bit to low bit
// the edge led is light when the bit is set 1
u8 code static_led[STATIC_LED_MAX] = {
	0x3f, // 0 => 0011 1111, dp,g is 0, others are 1
	0x06, // 1 => 0000 0110, c,b is 1, others are 0
	0x5b, // 2
	0x4f, // 3
	0x66, // 4
	0x6d, // 5
	0x7d, // 6
	0x07, // 7
	0x7f, // 8
	0x6f, // 9
	0x77, // A
	0x7c, // B
	0x39, // C
	0x5e, // D
	0x79, // E
	0x71, // F
};


#endif