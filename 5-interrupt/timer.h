#ifndef _TIMER_H_
#define _TIMER_H_

#include "types.h"

typedef void(*TimeOutPtr)();
typedef void(*TimerOutErrorPtr)(u8 error);

typedef void(*TimerInterrupt0Ptr)();
typedef void(*TimerInterrupt1Ptr)();

typedef struct Timer {
	u16 dalay_ms;
	u16 count;
	TimeOutPtr timeOut;
}Timer;

typedef struct TimerInterrput {
	BOOL t0_open_flag;
	BOOL t1_open_flag;
	TimerInterrupt0Ptr timerInterrupt0;
	TimerInterrupt1Ptr timerInterrupt1;
}TimerInterrput;

// T1 is not open, please do not use
void Init_Timer(BOOL t0_open, BOOL t1_open);
void Init_Timer_Interrupt(TimerInterrput* timerInterrupt);
void Delay_Timer_Mode1(Timer timer[], u8 timer_length, TimerOutErrorPtr errorPtr);

#endif
