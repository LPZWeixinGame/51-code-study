#include <reg52.h>

sbit LED_0 = P2^0;
sbit LED_1 = P2^1;

typedef unsigned short u16;
typedef unsigned char u8;

void delay_10us(u16 ten_us) {
	while(ten_us--);
}

// The led is light when P20 output lowV
// The left led is light when P20 sets 0 and others set 1
void main() {
	/*
	LED_0 = 0;
	LED_1 = 1;
	while(1) {
		delay_10us(5000);
		LED_0 = !LED_0;
		LED_1 = !LED_1;
	}
	*/
	
	int cnt = 0;
	while(1) {
		P2 = ~(0x01 << cnt);
		delay_10us(10000); // 50000 is equal to 450ms
		cnt++;
		if (cnt >= 9) {
			cnt = 0;
		}
	}
}