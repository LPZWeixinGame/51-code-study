#include <reg52.h>
#include "types.h"

#define DELAY_1_MS_COUNT 1000
#define HC_595_MAX_BITS 8
#define LED_MATRIX_LENGTH 8

// GND and OE are connected in j24.

sbit SRCLK = P3^6;
sbit CRCLK = P3^5;
sbit SER = P3^4;
sbit LED_7 = P2^7;

static u16 _count_1ms = 0;

static u8 code _led_matrix_buffer[LED_MATRIX_LENGTH] = {0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};

static u8 code _led_matrix_P0_buffer[LED_MATRIX_LENGTH] = {0xFF,0xE7,0xDB,0xBD,0x7E,0x66,0x99,0xFF};

void HC_595_Write(u8 dat);

// Start interrupt setting

void Init_Timer0_Interrupt() {
	// start timer0
	TR0 = 1;
	TMOD |= 0x01;
	
	// start timer0 interrupt
	EA = 1;
	ET0 = 1;
	TR0 = 1;
	
	TL0 = 0x18;
	TH0 = 0xFC;
}

static u8 _led_matrix_buffer_index = 0;
void Timer0_Interrupt_Run() interrupt 1 {
	TL0 = 0x18;
	TH0 = 0xFC;
	
	// 
	
	_count_1ms++;
	if (_count_1ms >= DELAY_1_MS_COUNT) {
		LED_7 = !LED_7;
		_count_1ms = 0;
	}
	
	HC_595_Write(0x00); // reset the buffer to 0x00, eliminate the residual
	HC_595_Write(_led_matrix_buffer[_led_matrix_buffer_index]);
	P0 = _led_matrix_P0_buffer[_led_matrix_buffer_index];
	_led_matrix_buffer_index++;
	if (_led_matrix_buffer_index >= 8) {
		_led_matrix_buffer_index = 0;
	}
}

void delay_10_us(u16 ten_us) {
	// ten_us = 1  =>  10us
	while(ten_us--);
}

// use 74hc595
void HC_595_Write(u8 dat) {
	u8 i = 0;
	for (; i < HC_595_MAX_BITS; i++) {
		SER = dat>>7;
		dat<<=1;
		SRCLK = 0;
		delay_10_us(1);
		SRCLK = 1;
		delay_10_us(1);
	}
	CRCLK = 0;
	delay_10_us(1);
	CRCLK = 1;
}

void main() {
	Init_Timer0_Interrupt();
	
	P0 = 0x00;
	while(1){
		/*
		u8 i;
		for (i = 0; i < LED_MATRIX_LENGTH; i++) {
			HC_595_Write(0x00); // reset the buffer to 0x00, eliminate the residual
			HC_595_Write(_led_matrix_buffer[i]);
			delay_10_us(50000);
		}
		*/
	}
}
