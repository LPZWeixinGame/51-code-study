#include "24C02.h"
#include "i2c.h"
#include "common.h"
#include "uart.h"

void AT24C02_WriteByte(u8 addr, u8 in_dat) {
    I2C_Start();
    I2C_WriteByte(0xA0);  // 发送写指令
    I2C_WaitACK();        // 等待写指令的 ack
    I2C_WriteByte(addr);  // 发送要写入 EEPRom 的地址
    I2C_WaitACK();        // 等待 EEPRom 返回 ack
    I2C_WriteByte(in_dat);// 往 EEPRom 写入数据，写完数据，EEPRom 的地址自动+1
    I2C_WaitACK();        // 等待写入数据完成的 ack
    I2C_Stop();
    
    Timer_SleepMS(10);
}

void AT24C02_ReadByte(u8 addr, u8* out_dat) {
    I2C_Start();
    I2C_WriteByte(0xA0);  // 发送写指令
    I2C_WaitACK();        // 等待写指令的 ack
    I2C_WriteByte(addr);  // 发送要写入 EEPRom 的地址
    I2C_WaitACK();        // 等待 EEPRom 返回 ack
    
    I2C_Start();          // 重新发送开启指令
    I2C_WriteByte(0xA1);  // 发送读指令，读取数据
    I2C_WaitACK();        // 等待读指令的 ack
    *out_dat = I2C_ReadByte();  // 读取数据，读完数据，EEPRom 的地址自动+1
    I2C_WriteNack();  // 不再读取数据，发送 nack 结束
    I2C_Stop();
    
    Timer_SleepMS(10);
}

// 0 -- 写入
// 1 -- 不能写入
static u8 AT24C02_WaitForReady() {
    u8 idata count = 0;
    u8 idata ack;

    do {
        I2C_Start();
        I2C_WriteByte(0xA0);  // 发送写指令
        ack = I2C_WaitACK();        // 等待写指令的 ack
        if (ack == 0) {
            // 可以写入
            I2C_Stop();
            return 0;
        }
        else {
            count++;
            if (count >= 5) {
                // 持续5次，50US 不能写入，就直接退出
                I2C_Stop();
                return 1;
            }
            Timer_Sleep10US(1);
        }
        I2C_Stop();
    } while(1);
}

u8 AT24C02_WriteBytes(u8 start_addr, u8* in_dat, u8 in_len) {
    u8 idata ack = 0;
    
    u8 i = 0, tmp;

    if (AT24C02_WaitForReady()) {
        // 不能写入
        return 1;
    }
    
    I2C_Start();
    I2C_WriteByte(0xA0);  // 发送写指令
    ack = I2C_WaitACK();        // 等待写指令的 ack
    if (ack != 0) {
        I2C_Stop();
        Timer_SleepMS(10);
        return ack;
    }
    I2C_WriteByte(start_addr);  // 发送要写入 EEPRom 的地址
    ack = I2C_WaitACK();        // 等待 EEPRom 返回 ack
    if (ack != 0) {
        I2C_Stop();
        Timer_SleepMS(10);
        return ack;
    }
    
    
    while (in_len--) {
        tmp = ++i + '0';
        Uart_WriteBuf(&tmp, 1); // 测试使用，发送 uart
        Uart_WriteBuf(" = ", 3); // 测试使用，发送 uart
        tmp = *in_dat;
        Uart_WriteBuf(&tmp, 1); // 测试使用，发送 uart
        
        I2C_WriteByte(*in_dat++);// 往 EEPRom 写入数据，写完数据，EEPRom 的地址自动+1
        ack = I2C_WaitACK();        // 等待写入数据完成的 ack
        if (ack != 0) {
            I2C_Stop();
            Timer_SleepMS(10);
            return ack;
        }
        Uart_WriteBuf("write ok! ", 10); // 测试使用，发送 uart
    }
    

    I2C_Stop();
    Timer_SleepMS(10);

    return ack;
}

u8 AT24C02_ReadBytes(u8 start_addr, u8* out_dat, u8 out_len) {
    u8 idata ack = 0;
    u8 idata i = 0;

    if (AT24C02_WaitForReady()) {
        // 不能写入
        return 1;
    }
    
    I2C_Start();
    I2C_WriteByte(0xA0);  // 发送写指令
    ack = I2C_WaitACK();        // 等待写指令的 ack
    if (ack != 0) {
        I2C_WriteNack();  // 不再读取数据，发送 nack 结束
        I2C_Stop();
        Timer_SleepMS(10);
        return ack;
    }
    I2C_WriteByte(start_addr);  // 发送要写入 EEPRom 的地址
    ack = I2C_WaitACK();        // 等待 EEPRom 返回 ack
    if (ack != 0) {
        I2C_WriteNack();  // 不再读取数据，发送 nack 结束
        I2C_Stop();
        Timer_SleepMS(10);
        return ack;
    }
    
    I2C_Start();          // 重新发送开启指令
    I2C_WriteByte(0xA1);  // 发送读指令，读取数据
    ack = I2C_WaitACK();        // 等待读指令的 ack
    if (ack != 0) {
        I2C_WriteNack();  // 不再读取数据，发送 nack 结束
        I2C_Stop();
        Timer_SleepMS(10);
        return ack;
    }

    for (i = 0; i < out_len; i++) {
        u8 tmp;
        tmp = i + 1 + '0';
        Uart_WriteBuf(&tmp, 1); // 测试使用，发送 uart
        Uart_WriteBuf(" = ", 3); // 测试使用，发送 uart
        
        *out_dat = I2C_ReadByte();// 往 EEPRom 写入数据，写完数据，EEPRom 的地址自动+1
        tmp = *out_dat;
        Uart_WriteBuf(&tmp, 1); // 测试使用，发送 uart
        out_dat++;
        I2C_WriteACK();  // 发送ACK
        Uart_WriteBuf(" read ok!\n", 10); // 测试使用，发送 uart
    }

    I2C_WriteNack();  // 不再读取数据，发送 nack 结束
    I2C_Stop();
    Timer_SleepMS(10);

    return ack;
}