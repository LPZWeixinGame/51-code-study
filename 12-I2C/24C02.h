#ifndef _24C02_H_
#define _24C02_H_

#include "types.h"

// 24C02 是 8K的数据为，addr 地址是从 0 - 255，256 个数据位

void AT24C02_WriteByte(u8 addr, u8 in_dat);

void AT24C02_ReadByte(u8 addr, u8* out_dat);

/*******************************************************************************
* 函 数 名         : AT24C02_WriteBytes
* 函数功能		   : 往 EEPRom 写入Bytes
* 输    入         : start_addr：开始地址
                     in_dat：输入的数据
                     in_len：输入的数据长度
* 输    出         : 1，写入不成功
        			 0，写入成功
*******************************************************************************/
u8 AT24C02_WriteBytes(u8 start_addr, u8* in_dat, u8 in_len);

/*******************************************************************************
* 函 数 名         : AT24C02_ReadBytes
* 函数功能		   : 从 EEPRom 读数据
* 输    入         : start_addr：开始地址
                     out_dat：输出的数据
                     out_len：输出的数据长度
* 输    出         : 1，写入不成功
        			 0，写入成功
*******************************************************************************/
u8 AT24C02_ReadBytes(u8 start_addr, u8* out_dat, u8 out_len);  // 读取有问题，只能读一位，发送完 ack，地址不能后移


#endif