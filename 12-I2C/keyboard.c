#include <reg52.h>
#include <string.h>
#include "types.h"
#include "keyboard.h"

sbit KEY_OUT_1 = P1^7;
sbit KEY_OUT_2 = P1^6;
sbit KEY_OUT_3 = P1^5;
sbit KEY_OUT_4 = P1^4;
sbit KEY_IN_1 = P1^3;
sbit KEY_IN_2 = P1^2;
sbit KEY_IN_3 = P1^1;
sbit KEY_IN_4 = P1^0;

typedef struct ButtonState {
	u8 key_state;
	BOOL key_press;
    BOOL key_send_release;
} ButtonState;

static ButtonState pdata _button_state[16];

static KeyBoardOnClickPtr _key_on_click = NULL;

void KeyBoard_Init(KeyBoardOnClickPtr on_click_ptr) {
    _key_on_click = on_click_ptr;
    
    memset(&_button_state, 0, 16*sizeof(ButtonState));
    
    // 初始化全部是高电平
    P1 = 0xFF;
}

void KeyBoard_Dirver() {
    u8 i, j, i_index, j_index;
    for (i = 0; i < 4; i++) {
        // 循环检测4x4的矩阵按钮
        i_index = i * 4;
        for (j = 0; j < 4; j++) {
            j_index = i_index+j;
            if (_button_state[j_index].key_send_release) {
                // 把send release 的标志位置为false
                _button_state[j_index].key_send_release = FALSE;
                
                // 检测到按钮动作
                // 按键按下时执行动作
                _key_on_click(i, j); // 调用按键动作
            }
        }
    }
}

// 按键扫描函数，需在定时终端中调用，推荐调用间隔1ms
void KeyBoard_Scan() {
    u8 i, index, i_index;
    static u8 _key_out = 0; // 矩阵按键扫描输出索引
    
    // 将一行的4个按键值移入缓冲区
    index = _key_out*4;
    _button_state[index++].key_state = (_button_state[index].key_state << 1) | KEY_IN_1;
    _button_state[index++].key_state = (_button_state[index].key_state << 1) | KEY_IN_2;
    _button_state[index++].key_state = (_button_state[index].key_state << 1) | KEY_IN_3;
    _button_state[index++].key_state = (_button_state[index].key_state << 1) | KEY_IN_4;
    
    // 消抖后更新按键状态
    index = _key_out*4;
    for (i = 0; i < 4; i++) {
        // 每行4个按键，所以循环4次
        i_index = index + i;
        if ((_button_state[i_index].key_state & 0x0F) == 0x00) {
            // 连续4次扫描值为0，即4x4ms内都是按下状态时，可认为按键已稳定的按下
            _button_state[i_index].key_press = TRUE;
        }
        else if ((_button_state[i_index].key_state & 0x0F) == 0x0F) {
            // 连续4次扫描值为0，即4x4ms内都是按下状态时，可认为按键已稳定的弹起
            if (_button_state[i_index].key_press) {
                _button_state[i_index].key_press = FALSE;
                _button_state[i_index].key_send_release = TRUE; // 开始发送 button 的click 事件，但是要从主循环中发送
            }
        }
    }
    
    // 执行下一次的扫描输出
    _key_out++;        // 输出索引递增 
    _key_out &= 0x03;  // 索引值加到4即归零
    switch(_key_out) { // 根据索引值，释放当前输出引脚，拉低下次的输出引脚
        case 0: KEY_OUT_4 = 1; KEY_OUT_1 = 0; break;
        case 1: KEY_OUT_1 = 1; KEY_OUT_2 = 0; break;
        case 2: KEY_OUT_2 = 1; KEY_OUT_3 = 0; break;
        case 3: KEY_OUT_3 = 1; KEY_OUT_4 = 0; break;
        default: break;
    }
}
