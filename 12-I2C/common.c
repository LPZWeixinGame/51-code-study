#include "common.h"

void Timer_SleepMS(u16 ms) {
    u16 i,j;
	for(i=ms;i>0;i--)
		for(j=110;j>0;j--);
}

void Timer_Sleep10US(u16 ten_us) {
    while(ten_us--);
}

u16 NumToString(u8* str, i32 num) {
    u8 pdata tmp1[16] = { 0 };
    u8 len_num = 0;
    char i;

    // 1. 计算
    do {
        tmp1[len_num++] = '0' + (num % 10);  // 使用asic 码，0是 48，后边数字都是+48
        num = num / 10;
    } while (num > 0 && len_num < 16);

    // 2. 翻转 tmp1
    for (i = len_num - 1; i >= 0; i--) {
        *str++ = tmp1[i];
    }

    *str = '\0';
    return len_num;
}

u16 BinaryToString(u8* str, u8 num) {
    u8 pdata tmp1[8] = { 0 };
    u8 len_num = 0;
    char i;

    // 1. 计算
    do {
        tmp1[len_num++] = '0' + (num % 2);  // 使用asic 码，0是 48，后边数字都是+48
        num = num / 2;
    } while (num > 0 && len_num < 8);

    for (i = 0; i < 8 - len_num; i++) {
        *str++ = '0';
    }

    // 2. 翻转 tmp1
    for (i = len_num - 1; i >= 0; i--) {
        *str++ = tmp1[i];
    }

    *str = '\0';
    return len_num;
}

void MemoryToString(u8* dst, u8* src, u8 src_len) {
    u8 tmp;
    while (src_len--) {
        tmp = *src >> 4;  // 先取高四位
        if (tmp <= 9) {
            *dst++ = tmp + '0';
        }
        else {
            *dst++ = tmp + 10 + 'A';
        }
        
        tmp = *src & 0x0F;  // 先取低四位
        if (tmp <= 9) {
            *dst++ = tmp + '0';
        }
        else {
            *dst++ = tmp + 10 + 'A';
        }
        *dst++ = ' ';
        src++;
    }
}
