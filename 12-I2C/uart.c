#include <reg52.h>
#include "uart.h"
#include "types.h"

static u8 _send_buff_complete = FALSE;
static u8 pdata _receive_buf[40];

void Uart_Init() {
    // 打开定时器1 和 中断1
    // TMOD |= 0x02;  // 定时器最好是在一个地方设置，外部已经设置
    
    SCON |= 0x50;
    PCON |= 0X80;
}

BOOL Uart_Read_Buf(u8* receBuf) {
    if (RI) {
        *receBuf = 0;
        RI = 0; // 手动清零接收中断标志位
        *receBuf = SBUF; // 接收到数据并保存到接收字节中
        return TRUE;
    }
    return FALSE;
}

u16 Uart_ConfigBand(u16 band) {
    //return 0XFA; // 11059200 频率的波特率， 0xFA = 250
    return 256 - (11059200/12/16)/band; // 计算方式
}

/*
u16 Uart_ReadBuf(u8* receBuf, u16 receLen) {
    (void*)receBuf;
    receLen = 0;
    return 0;
}
*/

void Uart_WriteBuf(u8* receBuf, u16 receLen) {
    while (receLen--) {
        _send_buff_complete = FALSE; // 重置发送完成
        SBUF = *receBuf++;
        while (!_send_buff_complete); // 等待发送完成
    }
}

void Uart_WriteMonitor() {
    if(TI) {
        _send_buff_complete = TRUE;
        TI = 0;
    }
}

void Uart_ReadMonitor(u8 ms) {
    ms = 0;
}
