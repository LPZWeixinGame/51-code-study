#include <reg52.h>
#include <STRING.h>
#include "types.h"
#include "uart.h"
#include "static_button_press.h"
#include "lcd_1602.h"
#include "keyboard.h"
#include "common.h"
#include "i2c.h"
#include "24C02.h"

static u8 T0RH = 0;
static u8 T0RL = 0;

static void led1_turn_on_off();
static void _Uart_ReceiveMsg();

static void Test24C02_Write(u8 addr, u8 dat);
static void Test24C02_Read(u8 addr, u8* dat);
static void Test24C02(u8 button_index);
static void Test24C02_Dat(u8 button_index);
static void Test24C02_Bytes(u8 button_index);

// Start interrupt setting

static u16 Timer1_Config_Delay(u16 ms) {
    u32 tmp;
    
    tmp = 11059200/12;  // 定时器计数频率
    tmp = (tmp * ms) / 1000;  // 计算所需要的计数
    tmp = 65536 - tmp;   // 计算定时器重载值
    tmp = tmp + 18;      // 补偿中断响应延时造成的误差
    
    return tmp;
}

void Timer0_Init_Interrupt() {
    u16 T0TH = Timer1_Config_Delay(1);
    
	// start timer0
	TMOD &= 0xF0;
    TMOD |= 0x01;
	
    T0RH = (u8)(T0TH >> 8);  // 加载T0 的重载值
    T0RL = (u8)(T0TH);
	TH0 = T0RH;
	TL0 = T0RL;

    // start timer0 interrupt
	EA = 1;
	ET0 = 1;
    TR0 = 1;
}


void Timer0_Interrupt_Run() interrupt 1 {
	TH0 = T0RH;
	TL0 = T0RL;
	//TL0 = 0x18;
	//TH0 = 0xFC;

    // 4个静态按钮点击
    ButtonKey_Scan();
    
    // 按钮矩阵点击
    KeyBoard_Scan();
    
    // 测试定时器中断是否有用
	led1_turn_on_off();
    
    // 监控
    Uart_ReadMonitor(1);
    
    // 查看
    _Uart_ReceiveMsg();
}

//sbit LED_7 = P2^7; // P2^7 是LCD 的使能引脚，设置1就要写数据了
sbit LED_1 = P2^0;
static void led1_turn_on_off() {
	static u16 led1_count = 0;
	led1_count++;
	if (led1_count >= 1000) {
		LED_1 = !LED_1;
		led1_count = 0;
	}
}

/// ------------------------------------
// 接收到urat消息：需要判断什么时候接收完毕。假设如果接收到消息后30ms 后都没有接收消息，就代表消息已经发送完毕

sbit LED_2 = P2^1;
sbit LED_3 = P2^2;

#define LCD1602_MAX_LENGTH 33

static u8 _Receive_Buf = 0;

static u8 xdata _uart_msg[LCD1602_MAX_LENGTH] = {0};
static u8 _uart_msg_len = 0;
static u8 _uart_receive_end = FALSE;
static u8 _uart_send_beyond_msg = FALSE;
static u16 _uart_receive_end_count = 0;

static void _Uart_ReceiveMsg() {
    _uart_receive_end_count++;
    if (_uart_receive_end_count >= 30) {
        // 1. 如果延迟30ms 没有消息，显示消息
        _uart_receive_end_count = 0;
        if(_uart_msg_len != 0 && _uart_receive_end == FALSE) {
            _uart_msg[_uart_msg_len] = 0;
            _uart_receive_end = TRUE;
        }
        return;
    }
    
    if (Uart_Read_Buf(&_Receive_Buf)) {
        // 如果已经接收完消息，再接收消息，就丢弃
        if (_uart_receive_end) {
            return;
        }

        // 2. 如果接收到消息
        _uart_receive_end_count = 0; // 重置计数
        
        // 2.1 如果接收到消息大于32个，需要返回PC，数据已经超了
        if (_uart_msg_len > LCD1602_MAX_LENGTH-2) { // -2 很重要，防止数组越界
            _uart_receive_end = TRUE;
            _uart_send_beyond_msg = TRUE;
            _uart_msg[_uart_msg_len] = 0;
            return;
        }
        
        _uart_msg[_uart_msg_len++] = _Receive_Buf;
    }
}

static void _Uart_SendBeyondMsg() {
    if (_uart_send_beyond_msg) {
        u8 code beyond_size_msg[] = "Beyond size";
        _uart_send_beyond_msg = FALSE;
        Uart_WriteBuf(beyond_size_msg, strlen(beyond_size_msg));
    }
}

// 在主线程中把消息显示出来
static void _Uart_ShowMsg() {
    // 如果开始刷新，并且有数据了
    if (_uart_receive_end) {
        _uart_receive_end = FALSE;
        _uart_receive_end_count = 0; // 重置计数
        
        if (_uart_msg_len != 0) {
            LCD1602_Clear();
            LCD1602_ShowData(0, 0, _uart_msg);
            
            // 从uart 传输的数据重新发送到 uart 中
            Uart_WriteBuf(_uart_msg, strlen(_uart_msg));
            
            // 重置
            _uart_msg_len = 0;
            memset(_uart_msg, 0, LCD1602_MAX_LENGTH);
            
            // msg 发送不完整，可能是没有使用双通道发送，在接收时候发送了，导致数据丢失
            _Uart_SendBeyondMsg();
        }
    }
}

/// ------------------------------------

static void Timer1_Init_Interrupt() {

    Uart_Init();
    
    // 如果使用定时器2，最好使用模式2
    TMOD &= 0x0f; // 清零T1 控制位
    TMOD |= 0x20; // 配置T1 的模式为2
    
    // 使用码率 9600
    TH1 = Uart_ConfigBand(9600);
    TL1 = TH1;
    
    ES = 1;
    EA = 1;  // 使能总中断设置为1
    TR1 = 1; // 最后设置定时器，是在保证上面设置参数后才打开定时器，防止定时器打开了还没有设置参数
}

sbit LED_6 = P2^6;
sbit LED_5 = P2^5;
sbit LED_4 = P2^4;


// 如果输入的1，6号灯开关；如果输入其他的，5号灯开关
static void _Change_LED() {
    if (_Receive_Buf == 6) {
        LED_6 = !LED_6;
    }
    else if (_Receive_Buf == 5) {
        LED_5 = !LED_5;
    }
    else {
        LED_4 = !LED_4;
    }
}

void Timer1_Interrupt_Run() interrupt 4 {
    
    _Uart_ReceiveMsg();
    Uart_WriteMonitor();
}

/// ------------------------------------

static u8 code _key_str_map[4][2] = {"1", "2", "3", "4"};

static void Button_Key_OnClick(u8 button_index) {
    
    // 清除所有
    LCD1602_Clear();
    //LCD1602_ShowData(0, 0, &_key_str_map[button_index]);
    
    //Uart_WriteBuf(&button_index, 1);
    // 按钮0 和串口重合，不能发送串口信息
    
    button_index = 0;
}

static void KeyBoard_OnClick(u16 x, u16 y) {
    LCD1602_Clear();
    //LCD1602_ShowData(0, 0, &_key_str_map[x]);
    //LCD1602_ShowData(0, 1, &_key_str_map[y]);
    
    if (x == 0) {
        Test24C02(y);
    }
    else if (x == 1) {
        Test24C02_Dat(y);
    }
    else if (x == 2) {
        Test24C02_Bytes(y);
    }
    
    /*
    if (x == 1 && y == 0) {
        LCD1602_ShowData(0, 0, "xixi xiao bang bang");
    }
    
    if (x == 1 && y == 2) {
        LCD1602_ShowData(0, 0, "Linlin xiao bang bang");
    }
    */
}

// --------------------------------------
// 测试发送 i2c

static void TestI2C_1() {
    u8 idata ack = 0;
    u8 xdata str[10];
    
    // start
    I2C_Start();
    
    
    // 地址是 OxA0，1010000 0，最后的0位是写入标志
    I2C_WriteByte(0xA0);
    ack = I2C_WaitACK();
    
    // stop
    I2C_Stop();

    str[0] = '0';
    str[1] = 'x';
    str[2] = 'A';
    str[3] = '0';
    str[4] = ' ';
    str[5] = 'a';
    str[6] = 'c';
    str[7] = 'k';
    str[8] = ack + '0';
    str[9] = '\0';
    
    LCD1602_ShowData(0, 0, str);
}

static void TestI2C_2() {
    u8 idata ack = 0;
    u8 xdata str[10];
    
    // start
    I2C_Start();
    
    
    // 地址是 OxA0，101000 0，最后的0位是写入标志
    I2C_WriteByte(0xA0);
    ack = I2C_WaitACK();
    
    // stop
    I2C_Stop();

    str[0] = '0';
    str[1] = 'x';
    str[2] = 'A';
    str[3] = '1';
    str[4] = ' ';
    str[5] = 'a';
    str[6] = 'c';
    str[7] = 'k';
    str[8] = ack + '0';
    str[9] = '\0';
    
    LCD1602_ShowData(0, 1, str);
}

static void Test24C02_Write(u8 addr, u8 dat) {
    u8 xdata str[10];
    
    AT24C02_WriteByte(addr, dat);

    str[0] = 'w';
    str[1] = 'r';
    str[2] = 'i';
    str[3] = 't';
    str[4] = 'e';
    str[5] = ' ';
    str[6] = addr + '0';
    str[7] = ' ';
    str[8] = dat + '0';
    str[9] = '\0';

    LCD1602_ShowData(0, 0, str);
    
    Timer_SleepMS(1);
}

static void Test24C02_Read(u8 addr, u8* dat) {
    u8 xdata str[9];
    
    AT24C02_ReadByte(addr, dat);

    str[0] = 'r';
    str[1] = 'e';
    str[2] = 'a';
    str[3] = 'd';
    str[4] = ' ';
    str[5] = addr + '0';
    str[6] = ' ';
    str[7] = *dat + '0';
    str[8] = '\0';
    
    LCD1602_ShowData(0, 1, str);
    
    Timer_SleepMS(1);
}


static u8 dat_24C02 = 0;
static void Test24C02(u8 button_index) {
    switch(button_index){
        case 0:
            Test24C02_Read(0, &dat_24C02); // 问题
            break;
        case 1:
            Test24C02_Write(0, dat_24C02);
            break;
        case 2:
            Test24C02_Read(1, &dat_24C02);
            break;
        case 3:
            Test24C02_Write(1, dat_24C02);
            break;
    }
}

static void Test24C02_Dat(u8 button_index) {
    u8 xdata str[8];
    switch(button_index){
        case 0:
        {
            // 清零
            dat_24C02 = 0;
        }
            break;
        case 1:
        {
            // 累计+1
            dat_24C02++;
        }
            break;
    }
    
    str[0] = 'd';
    str[1] = 'a';
    str[2] = 't';
    str[3] = ' ';
    str[4] = '=';
    str[5] = ' ';
    str[6] = dat_24C02 + '0';
    str[7] = '\0';
    
    LCD1602_ShowData(0, 0, str);
}

static void Test24C02_Bytes(u8 button_index) {
    u8 xdata str[8];
    switch(button_index){
        case 0:
        {
            // 写数据
            u8 i = 0;
            for (; i < 7; i++) {
                str[i] = i + 1 + '0';
            }
            str[7] = '\0';
            if (AT24C02_WriteBytes(0, str, 7) == 0) {
                LCD1602_ShowData(0, 0, str);
            }
            else {
                LCD1602_ShowData(0, 0, "write error");
            }
        }
            break;
        case 1:
        {
            // 读数据
            if (AT24C02_ReadBytes(0, str, 7) == 0) {
                str[7] = '\0';
                LCD1602_ShowData(0, 1, str);
            }
            else {
                LCD1602_ShowData(0, 1, "read error");
            }
        }
            break;
    }
    

    
}

// --------------------------------------

void main() {
	u8 code print_msg[] = "LinXi Family";
    
    Timer0_Init_Interrupt();
    Timer1_Init_Interrupt();
    
    ButtonKey_Init(Button_Key_OnClick);
    KeyBoard_Init(KeyBoard_OnClick);
    
    LCD1602_Init();
    
    LCD1602_ShowData(0, 0, "hello world");
    LCD1602_ShowData(0, 1, print_msg);

    //TestI2C_1();
    //TestI2C_2();
	
	while(1) {
        // 做频繁闪烁和轮询的动作
        // 矩阵按钮做轮询操作
        KeyBoard_Dirver();
        
        // 单点按钮做轮询操作
        ButtonKey_Dirver();
        
        _Uart_ShowMsg();
	}
}
