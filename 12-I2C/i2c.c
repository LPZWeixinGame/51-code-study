#include <reg52.h>
#include "i2c.h"
#include "common.h"

sbit I2C_SCL = P2^1;
sbit I2C_SDA = P2^0;

void I2C_Start() {
    I2C_SDA = 1;  // SDA 和 SCL 要置1，但 SDA 需要先设置
    I2C_SCL = 1;
    Timer_Sleep10US(1); // 延时等待所有设置完成
    I2C_SDA = 0;  // SDA 设置下降沿，start 开始
    Timer_Sleep10US(1);
    I2C_SCL = 0;  // SCL 设置成下降沿，准备发送和接收数据
    Timer_Sleep10US(1); // 设置时间，等待所有时间完成
}

void I2C_Stop() {
    I2C_SDA = 0;  // SDA 和 SCL 要置0，但 SDA 需要先设置
    I2C_SCL = 0;
    Timer_Sleep10US(1); // 延时等待所有设置完成
    I2C_SCL = 1;  // SCL 设置上升沿，stop 开始
    Timer_Sleep10US(1);
    I2C_SDA = 1;  // SDA 设置成上升沿，准备停止
    Timer_Sleep10US(1); // 设置时间，等待所有时间完成
}

void I2C_WriteByte(u8 dat) {
    u8 mask = 0x80;
    
    I2C_SCL = 0; // 先设置下降沿，等待发送数据
    Timer_Sleep10US(1); // 等待上面数据准备好
    for (; mask != 0; mask >>= 1) {
        if ((dat & mask) == 0) {
            // dat & mask 能获取到当前要发送的位，如果为0
            I2C_SDA = 0;
        }
        else {
            I2C_SDA = 1;
        }
        
        Timer_Sleep10US(1); // 等待上面数据准备好
        I2C_SCL = 1; // 设置SCL 上升沿，准备发送数据
        Timer_Sleep10US(1); // 等待上升沿完成，发送数据
        I2C_SCL = 0; // 设置SCL 下降沿，发送完数据，准备下次发送
        Timer_Sleep10US(1); // 释放，重置
    }
}

u8 I2C_ReadByte() {
    u8 dat = 0, i;
    
    for (i = 0; i < 8; i++) {
        I2C_SCL = 0; // 先设置下降沿，准备读取数据
        Timer_Sleep10US(1); // 等待数据准备好
        I2C_SCL = 1; // 设置SCL 上升沿，读取数据
        Timer_Sleep10US(1); // 等待读取数据完成
        dat <<= 1;
        dat = dat | I2C_SDA;  // 读取到数据，放在 SDA 中
    }
    
    I2C_SCL = 0;   // 释放SCL
    return dat;
}

u8 I2C_WaitACK() {
    // 发送完成数据后，把SCL 设置到上升沿去接收 ack 数据
    // 如果接收到发送接收到的数据，如果没有接收到返回错误
    
    u8 time_temp=0;
    
    I2C_SDA = 1;   // 数据发送或者接收完成，释放
    Timer_Sleep10US(1);
    I2C_SCL = 1;   // 设置上升沿，接收ack 数据
    Timer_Sleep10US(1); // 等待接收ack 数据完成
    while (I2C_SDA) {
        time_temp++;
        if (time_temp > 100) {
            // 如果连续100次都是0，接收到nack 或者没有收到信息
            return 1;
        }
    }
    I2C_SCL = 0;   // 释放SCL
    return 0;
}

void I2C_WriteNack() {
    I2C_SCL = 0;  // 重置SCL和SDA = 1 nack
    I2C_SDA = 1;
    Timer_Sleep10US(1); // 等待重置完成
    I2C_SCL = 1;  // 设置上升沿发送 nack数据
    Timer_Sleep10US(1); // 等待发送完成
    I2C_SCL = 0;   // 释放SCL
}

void I2C_WriteACK() {
    I2C_SCL = 0;  // 重置SCL和SDA = 0 ack
    I2C_SDA = 0;
    Timer_Sleep10US(1); // 等待重置完成
    I2C_SCL = 1;  // 设置上升沿发送 nack数据
    Timer_Sleep10US(1); // 等待发送完成
    I2C_SCL = 0;   // 释放SCL
}
