#include <reg52.h>
#include <string.h>
#include "types.h"
#include "lcd_1602.h"
#include "common.h"

sbit _LCD1602_RW = P2^5;
sbit _LCD1602_RS = P2^6;
sbit _LCD1602_EN = P2^7;

#define _LCD1602_DB P0

#define _LCD1602_8_BIT 1
#define _LCD1602_NO_WRAP 0

#ifdef _LCD1602_8_BIT

static void _LCD1602_WaitForReady() {
    u8 sta7 = 0;

    // 读状态：RS=L, R/W=H, E=H，读D7的状态，1是禁止；0是允许
    _LCD1602_DB = 0xFF;
    _LCD1602_EN = 0;
    _LCD1602_RS = 0;
    _LCD1602_RW = 1;
    
    // sta7 是 D7 位，最高位，如果是1 的话就禁止， 0x10000000
    do {
        _LCD1602_EN = 1;
        sta7 = _LCD1602_DB;
        _LCD1602_EN = 0;
    } while (sta7 & 0x80);
}

static void _LCD1602_WriteData(u8 msg) {
    _LCD1602_WaitForReady(); // 等待可以写入指令
    
    // 写指令：RS=H, R/W=L, E=H，D0-D7=指令码，E=高脉冲
    _LCD1602_EN = 0;
    _LCD1602_RS = 1;
    _LCD1602_RW = 0;
    _LCD1602_DB = msg;
    // EN指令的升上沿和下降沿的时间是 25ns，如果一个指令执行的周期小于25ns，需要增加delay 1ms 时间
    // delay 1ms
    Timer_SleepMS(1); // 51 单片机延迟没有问题，但STM32 执行命令快，可能会有问题，需要延迟1ms
    _LCD1602_EN = 1; // 高脉冲执行
    // delay 1ms
    Timer_SleepMS(1);
    _LCD1602_EN = 0; // 执行完指令后需要释放，拉低电压
}

static void _LCD1602_WriteCmd(u8 cmd) {
    _LCD1602_WaitForReady(); // 等待可以写入指令
    
    // 写指令：RS=L, R/W=L, E=H，D0-D7=指令码，E=高脉冲
    _LCD1602_EN = 0;
    _LCD1602_RS = 0;
    _LCD1602_RW = 0;
    _LCD1602_DB = cmd;
    // EN指令的升上沿和下降沿的时间是 25ns，如果一个指令执行的周期小于25ns，需要增加delay 1ms 时间
    // delay 1ms
    Timer_SleepMS(1); // 51 单片机延迟没有问题，但STM32 执行命令快，可能会有问题，需要延迟1ms
    _LCD1602_EN = 1; // 高脉冲执行
    // delay 1ms
    Timer_SleepMS(1);
    _LCD1602_EN = 0; // 执行完指令后需要释放，拉低电压
}

void LCD1602_Init() {
    _LCD1602_WriteCmd(0x38); // 显示模式设置 0011 1000 = 0x38。 设置16x2 显示，5x7 点阵，8位数据接口
    _LCD1602_WriteCmd(0x0C); // 显示开和光标不设置 0000 1DCB 这个设置，设置为 0000 1100 = 0x0C
    _LCD1602_WriteCmd(0x06); // 0000 01NS 设置为 0000 0110 = 0x06
    _LCD1602_WriteCmd(0x01); // 显示清屏：数值指针清零，所有显示清零
}

BOOL LCD1602_ShowData(u8 x, u8 y, u8* msg) {
    u16 msgLen = strlen(msg);
    
    // 如果 x > 16 或者 y > 1，是设置超了，直接返回错误 FALSE
    if (x > 15 || y > 1) {
        return FALSE;
    }

#if _LCD1602_NO_WRAP
    {
        u8 addr = 0;
        if (y == 0) {
            addr = (0x00 + x) | 0x80;
        }
        else {
            addr = (0x40 + x) | 0x80;
        }
        _LCD1602_WriteCmd(addr);
        
        while (msgLen--) {
            _LCD1602_WriteData(*msg++);
        }
    }
    return TRUE;
#else    
    if (y == 0) {
        // 第一行
        u8 index = 0;
        u8 i = 0;
        // *msg != '\0' 这里可以使用 '\0'和0，判断最后一位，不能使用"\0"
        while (*msg != 0) {
            index = x+i;
            if (index < 16) {
                // 第一行显示
                _LCD1602_WriteCmd(0x80+index);
            }
            else {
                // 第二行显示
                _LCD1602_WriteCmd(0x80+0x40+index-16);
            }
            
            _LCD1602_WriteData(*msg);
            // 往前移动
            msg++;
            i++;
        }
    }
    else {
        // 第二行
        u8 index = 0;
        u8 i = 0;
        while (*msg != 0) {
            index = x+i;
            if (index < 16) {
                // 第二行显示
                _LCD1602_WriteCmd(0x80+0x40+index);
            }
            else {
                // 第一行显示
                _LCD1602_WriteCmd(0x80+index-16);
            }
            
            _LCD1602_WriteData(*msg);
            // 往前移动
            msg++;
            i++;
        }
    }
    return TRUE;
#endif
}

void LCD1602_Clear() {
    _LCD1602_WriteCmd(0x01); // 显示清屏：数值指针清零，所有显示清零
}

void LCD1602_ShowChar(u8 x, u8 y, u8 msg) {
    u8 pdata tmp[2] = {0, 0};
    tmp[0] = msg;
    LCD1602_ShowData(x, y, tmp);
}

void LCD1602_ShowNum(u8 x, u8 y, i32 num, u8 showLenNum) {
    u8 pdata tmp1[17];
    u8 strLen;
    strLen = NumToString(tmp1, num);

    // 最后一位为0
    if (strLen > showLenNum) {
        strLen = showLenNum;
    }
    tmp1[strLen] = 0;

    LCD1602_ShowData(x, y, tmp1);
}

void LCD1602_ShowBinary(u8 x, u8 y, u8 num) {
    u8 pdata tmp[9] = {0};
    BinaryToString(tmp, num);
    LCD1602_ShowData(x, y, tmp);
}

#endif


