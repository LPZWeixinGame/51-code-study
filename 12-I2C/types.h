#ifndef _TYPES_H_
#define _TYPES_H_

#include <reg52.h>

typedef unsigned short u16;
typedef unsigned char  u8;
typedef unsigned long  u32;
typedef long           i32;

#define BOOL u8
#define TRUE 1
#define FALSE 0

#endif
