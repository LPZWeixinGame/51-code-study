#include <reg52.h>

typedef unsigned short u16;
typedef unsigned char  u8;
typedef void(*TimeOutPtr)();

#define SHORT_MAX 65535

sbit LED_0 = P2^0;
u8 cnt = 0;

void Init_Timer() {
	TR0 = 1;
}

void Init_LED() {
	
}

void Delay_Timer_Mode1(u16 dalay_ms, TimeOutPtr timeOut) {
	u16 index = 0;
	if (dalay_ms <= 0) {
		timeOut();
		return;
	}
	
	// 12MHz, 1ms = 1000 count, u16 = 65536, TH0+TL0 = 65536-1000 = 64536, 0xFC18, TH0 = 0xFC, TL0 = 0x18
	TL0 = 0xFC;
	TH0 = 0x18;
	while (1) {
		if (1 == TF0) {
			TF0 = 0;
			TL0 = 0xFC;
			TH0 = 0x18;
			index++;
			if (index >= dalay_ms) {
				timeOut();
				return;
			}
		}
	}
}

void TimeOutRun() {
	//LED_0 = !LED_0;
	P2 = ~(0x01 << cnt);
	cnt++;
	if (cnt >= 8) {
		cnt = 0;
	}
}

void main() {
	u16 time_out_ms = 1000;
	
	Init_LED();
	Init_Timer();
	
	while (1) {
		Delay_Timer_Mode1(time_out_ms, TimeOutRun);
	}
}