#ifndef _HC_74_138_H_
#define _HC_74_138_H_

#include "types.h"

void Init_HC_74_138();

void Change_HC_74_138(u8 dst_index);

#endif