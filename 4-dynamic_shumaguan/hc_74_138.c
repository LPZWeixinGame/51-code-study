#include <reg52.h>
#include "types.h"
#include "hc_74_138.h"

sbit A0 = P2^2;
sbit A1 = P2^3;
sbit A2 = P2^4;

void Init_HC_74_138() {
	// The 74HC138 is always open in my 51.
}

void Change_HC_74_138(u8 dst_index) {
	switch(dst_index) {
		case 1:
			A0 = 0; A1 = 0; A2 = 0;
			break;
		case 2:
			A0 = 1; A1 = 0; A2 = 0;
			break;
		case 3:
			A0 = 0; A1 = 1; A2 = 0;
			break;
		case 4:
			A0 = 1; A1 = 1; A2 = 0;
			break;
		case 5:
			A0 = 0; A1 = 0; A2 = 1;
			break;
		case 6:
			A0 = 1; A1 = 0; A2 = 1;
			break;
		case 7:
			A0 = 0; A1 = 1; A2 = 1;
			break;
		case 8:
			A0 = 1; A1 = 1; A2 = 1;
			break;
	}
}