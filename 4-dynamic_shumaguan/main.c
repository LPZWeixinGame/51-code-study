#include <reg52.h>
#include <stdlib.h>
#include "types.h"
#include "timer.h"
#include "static_led.h"
#include "hc_74_138.h"

#define DYNAMIC_LED_LENGTH 8

sbit LED_1 = P2^1;
sbit LED_7 = P2^7;

unsigned long sec = 12345678;
u8 dynamic_led_index = 0;

u8 LedBuffer[DYNAMIC_LED_LENGTH] = {0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff};

// timer1 = (Timer*)malloc(sizeof(Timer)*2); timer1 is NULL. malloc is not useful.

Timer timer[2];

// dynamic led
// use 74HC138 and 74HC245

// reflush the dynamic led per 1 microsecond
void Dynamic_LED_1_TimeOut() {
	// reflush
	P0 = 0x00; // changing the 138 from 1 to 2, there is some temp state to cause some shadow
	Change_HC_74_138(dynamic_led_index+1);
	P0 = LedBuffer[dynamic_led_index];
	
	dynamic_led_index++;
	if (dynamic_led_index >= DYNAMIC_LED_LENGTH) {
		dynamic_led_index = 0;
	}
}

// the stack may be full. need to use printf the log
// reflush the led number per 1 second
u8 shift = 2;
void Dynamic_LED_1000_TimeOut() {
	u8 index = 0;
	u8 shift1;

	LED_7 = !LED_7;
	shift++;

	for (; index < DYNAMIC_LED_LENGTH; index++) {
		shift1 = shift + index;
		LedBuffer[index] = static_led[shift1];
	}
	
	if (shift >= 8) {
		shift = 0;
	}
}

void Dynamic_LED_TimerOutError(u8 err) {
	err = 1;
	//LED_7 = 1;
}

void Init_Dynamic_Timer() {
	timer[0].dalay_ms = 1;
	timer[0].count = 0;
	timer[0].timeOut = Dynamic_LED_1_TimeOut;
	
	timer[1].dalay_ms = 1000;
	timer[1].count = 0;
	timer[1].timeOut = Dynamic_LED_1000_TimeOut;
}

void Init_Dynamic_Led() {
	u8 index = 0;
	unsigned long shift;
	for (shift = 1; index < DYNAMIC_LED_LENGTH; index++, shift*=10) {
		LedBuffer[index] = static_led[sec/shift%10];
	}
}

void Test_Dynamic() {
	Init_Dynamic_Timer();
	Init_Dynamic_Led();
	
	Init_HC_74_138();
	while (1) {
		Delay_Timer_Mode1(timer, 2, Dynamic_LED_TimerOutError);
	}
}

void main() {
	Init_Timer();
	Test_Dynamic();
}
