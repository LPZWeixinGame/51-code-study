#include <reg52.h>
#include <stdlib.h>
#include "types.h"
#include "timer.h"
#include "static_led.h"
#include "hc_74_138.h"

/*
u8 cnt = 0;

// static led
void Static_LED_TimeOutRun() {
	P0 = static_led[cnt];
	cnt++;
	if (cnt >= STATIC_LED_MAX-1) {
		cnt = 0;
	}
}

Timer timer[1];
void Init_Static_Timer() {
	timer[0].dalay_ms = 200;
	timer[0].count = 0;
	timer[0].timeOut = Static_LED_TimeOutRun;
}

void Test_Static() {
  Init_Static_Timer();
	while(1) {
		Delay_Timer_Mode1(timer, 1, NULL);
	}
}
*/

// dynamic led
// use 74HC138 and 74HC245

u8 cnt = 0;

sbit LED_0 = P2^0;
sbit LED_1 = P2^1;
sbit LED_2 = P2^2;
void Dynamic_LED_100_TimeOut() {
	P0 = static_led[cnt];
	cnt++;
	if (cnt >= STATIC_LED_MAX-1) {
		cnt = 0;
	}
}

void Dynamic_LED_1000_TimeOut() {
	LED_1 = !LED_1;
}

void Dynamic_LED_TimerOutError(u8 err) {
	err = 1;
	LED_2 = 1;
}

// timer1 = (Timer*)malloc(sizeof(Timer)*2); timer1 is NULL. malloc is not useful.

Timer timer[2];
void Init_Dynamic_Timer() {
	timer[0].dalay_ms = 500;
	timer[0].count = 0;
	timer[0].timeOut = Dynamic_LED_100_TimeOut;
	
	timer[1].dalay_ms = 1000;
	timer[1].count = 0;
	timer[1].timeOut = Dynamic_LED_1000_TimeOut;
}

void Test_Dynamic() {
	Init_Dynamic_Timer();
	
	Init_HC_74_138();
	while (1) {
		Delay_Timer_Mode1(timer, 2, Dynamic_LED_TimerOutError);
	}
}

void main() {
	Init_Timer();
	//Test_Static();
	Test_Dynamic();
}
