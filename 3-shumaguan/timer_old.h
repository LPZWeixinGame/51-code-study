#ifndef _TIMER_H_
#define _TIMER_H_

typedef void(*TimeOutPtr)();
typedef void(*TimerOutErrorPtr)(u8 error);

typedef struct Timer {
	u16 dalay_ms;
	BOOL  finish_flag;
	TimeOutPtr timeOut;
}Timer;

void Init_Timer();
void Delay_Timer_Mode1(Timer timer[], u8 timer_length, TimerOutErrorPtr errorPtr);

#endif
