#include <reg52.h>
#include <stdlib.h>
#include "types.h"
#include "timer.h"

#define SHORT_MAX 65535

void Init_Timer() {
	TR0 = 1;
}

void Delay_Timer_Mode1(Timer timer[], u8 timer_length, TimerOutErrorPtr errorPtr) {
	u8 i = 0;
	if (timer_length <= 0) {
		if (errorPtr != NULL) {
			errorPtr(-1);
		}
		return;
	}
	
	// 12MHz, 1ms = 1000 count, u16 = 65536, TH0+TL0 = 65536-1000 = 64536, 0xFC18, TH0 = 0xFC, TL0 = 0x18
	TL0 = 0xFC;
	TH0 = 0x18;
	while (1) {
		if (1 == TF0) {
			TF0 = 0;
			TL0 = 0xFC;
			TH0 = 0x18;

			for (i = 0; i < timer_length; i++) {
				timer[i].count++;
				if (timer[i].count >= timer[i].dalay_ms) {
					timer[i].timeOut();
					timer[i].count = 0;
				}
			}
		}
	}
}
