#include <reg52.h>
#include <stdlib.h>
#include "types.h"
#include "timer.h"

#define SHORT_MAX 65535

void Init_Timer() {
	TR0 = 1;
}

void Delay_Timer_Mode1(Timer timer[], u8 timer_length, TimerOutErrorPtr errorPtr) {
	u16 interval = 0;
	u8 finish_length = 0;
	u8 i = 0;
	if (timer_length <= 0) {
		if (errorPtr != NULL) {
			errorPtr(-1);
		}
		return;
	}
	
	// 12MHz, 1ms = 10000 count, u16 = 65536, TH0+TL0 = 65536-10000 = 55536, 0xD8EF, TH0 = 0xD8, TL0 = 0xEF
	TL0 = 0xEF;
	TH0 = 0xD8;
	while (1) {
		if (1 == TF0) {
			TF0 = 0;
			TL0 = 0xEF;
			TH0 = 0xD8;
			interval++;

			for (i = 0; i < timer_length; i++) {
				if (!timer[i].finish_flag && interval >= timer[i].dalay_ms) {
					timer[i].timeOut();
					timer[i].finish_flag = TRUE;
					finish_length++;
				}
				
				if (finish_length == timer_length) {
					for (i = 0; i < timer_length; i++) {
						timer[i].finish_flag = FALSE;
					}
					return;
				}
			}
		}
	}
}
