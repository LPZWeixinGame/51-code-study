#include "static_button_press.h"
#include <reg52.h>
#include <string.h>
#include "types.h"

typedef struct ButtonState {
	u8 key_state;
	BOOL key_press;
} ButtonState;

static ButtonKeyOnClickPtr _key_on_click = NULL;
static ButtonState _button_state[4];

// static key
sbit _Button_Key_1 = P3^1;
sbit _Button_Key_2 = P3^0;
sbit _Button_Key_3 = P3^2;
sbit _Button_Key_4 = P3^3;

static u16 led_show = 0;

static void On_Key_Press(u8 button_index) {
	_button_state[button_index].key_press = TRUE;
}

static void On_Key_Release(u8 button_index) {
	if (_button_state[button_index].key_press) {
		_button_state[button_index].key_press = FALSE;
        if (_key_on_click) {
            _key_on_click(button_index);
        }
	}
}

static void On_Key_Check(u8 button_index, u8 key) {
	// delete the jitter when press or release button.
	// 11111111111111 0101 00000000000000 01010 111111111111111
	// there is release state when 8 consecutive occurrences of 1. 
	// there is press state when 8 consecutive occurrences of 0. 
	// According to the instruction map, the electric truns on after pressing, input is 0. If off, input is 1.
	_button_state[button_index].key_state = (_button_state[button_index].key_state << 1) | key;
	if (_button_state[button_index].key_state == 0xff) {
		On_Key_Release(button_index);
	}
	else if (_button_state[button_index].key_state == 0x00) {
		On_Key_Press(button_index);
	}
}

void Button_Key_Init(ButtonKeyOnClickPtr on_click_ptr) {
	memset(&_button_state, 0, 4*sizeof(ButtonState));
    _key_on_click = on_click_ptr;
}

void Button_Key_Check() {
	On_Key_Check(0, _Button_Key_1);
	On_Key_Check(1, _Button_Key_2);
	On_Key_Check(2, _Button_Key_3);
	On_Key_Check(3, _Button_Key_4);
}
