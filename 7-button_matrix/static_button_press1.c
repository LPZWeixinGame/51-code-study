#include "static_button_press.h"
#include <reg52.h>
#include <string.h>
#include "types.h"
#include "hc_74_138.h"
#include "static_led.h"

typedef struct ButtonState {
	u8 key_state;
	BOOL key_press;
} ButtonState;

static ButtonState _button_state[4];

static void static_key();
static void led_7_turn_on_off();
static void init_button_state();

static void init_button_state() {
	memset(&_button_state, 0, 4*sizeof(ButtonState));
}

// Start interrupt setting

void Init_Timer0_Interrupt() {
	// start timer0
	TR0 = 1;
	TMOD |= 0x01;
	
	// start timer0 interrupt
	EA = 1;
	ET0 = 1;
	TR0 = 1;
	
	TL0 = 0x18;
	TH0 = 0xFC;
}


void Timer0_Interrupt_Run() interrupt 1 {
	TL0 = 0x18;
	TH0 = 0xFC;

	static_key();
	led_7_turn_on_off();
}

sbit LED_7 = P2^7;
static void led_7_turn_on_off() {
	static u16 led_7_count = 0;
	led_7_count++;
	if (led_7_count >= 1000) {
		LED_7 = !LED_7;
		led_7_count = 0;
	}
}


// static key
sbit _Button_Key_1 = P3^1;
sbit _Button_Key_2 = P3^0;
sbit _Button_Key_3 = P3^2;
sbit _Button_Key_4 = P3^3;

static u16 led_show = 0;

static void On_Key_Press(u8 button_index) {
	_button_state[button_index].key_press = TRUE;
}

static void On_Key_Release(u8 button_index) {
	if (_button_state[button_index].key_press) {
		_button_state[button_index].key_press = FALSE;
		led_show = button_index+1;
	}
}

static void On_Key_Check(u8 button_index, u8 key) {
	// delete the jitter when press or release button.
	// 11111111111111 0101 00000000000000 01010 111111111111111
	// there is release state when 8 consecutive occurrences of 1. 
	// there is press state when 8 consecutive occurrences of 0. 
	// According to the instruction map, the electric truns on after pressing, input is 0. If off, input is 1.
	_button_state[button_index].key_state = (_button_state[button_index].key_state << 1) | key;
	if (_button_state[button_index].key_state == 0xff) {
		On_Key_Release(button_index);
	}
	else if (_button_state[button_index].key_state == 0x00) {
		On_Key_Press(button_index);
	}
}

static void static_key() {
	On_Key_Check(0, _Button_Key_1);
	On_Key_Check(1, _Button_Key_2);
	On_Key_Check(2, _Button_Key_3);
	On_Key_Check(3, _Button_Key_4);
}

void Static_Button_Press_Test() {
	u8 dynamic_led_index = 0;
	
	init_button_state();
	Init_Timer0_Interrupt();
	Init_HC_74_138();

	led_show = 0;
	while(1){
		
		// reflush
		P0 = 0x00; // changing the 138 from 1 to 2, there is some temp state to cause some shadow
		dynamic_led_index = 0;
		Change_HC_74_138(dynamic_led_index+1);
		P0 = static_led[led_show];
		/*
		// reflush
		P0 = 0xff; // changing the 138 from 1 to 2, there is some temp state to cause some shadow
		dynamic_led_index = 0;
		Change_HC_74_138(dynamic_led_index+1);
		P0 = static_led[2];
		
		dynamic_led_index++;
		if (dynamic_led_index >= DYNAMIC_LED_LENGTH) {
			dynamic_led_index = 0;
		}
		*/
	}
}