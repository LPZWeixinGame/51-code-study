#ifndef _STATIC_BUTTON_PRESS_H_
#define _STATIC_BUTTON_PRESS_H_

#include "types.h"

typedef void (*ButtonKeyOnClickPtr)(u8 button_index);

void Button_Key_Init(ButtonKeyOnClickPtr on_click_ptr);

void Button_Key_Check();

#endif