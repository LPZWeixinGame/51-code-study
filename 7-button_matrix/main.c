#include <reg52.h>
#include "static_button_press.h"
#include "types.h"
#include "hc_74_138.h"
#include "static_led.h"

/*

#define DYNAMIC_LED_LENGTH 8

sbit LED_1 = P2^1;
sbit LED_7 = P2^7;

unsigned long sec = 12345678;
u8 dynamic_led_index = 0;

u8 LedBuffer[DYNAMIC_LED_LENGTH] = {0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff};

void Dynamic_Key();


// finish the dynamic key
// static key

static u8 key_s1_state = 0;
static BOOL key_s1_press = FALSE;

static void On_Key_S1_Press() {
	key_s1_press = TRUE;
}

static void On_Key_S1_Release() {
	if (key_s1_press) {
		key_s1_press = FALSE;
		led_show++;
		if (led_show >= 9) {
			led_show = 0;
		}
	}
}

void Dynamic_Key() {
	// delete the jitter when press or release button.
	// 11111111111111 0101 00000000000000 01010 111111111111111
	// there is press state when 8 consecutive occurrences of 1. 
	// there is release state when 8 consecutive occurrences of 0. 
	key_s1_state = (key_s1_state << 1) | Key_1;
	if (key_s1_state == 0xff) {
		On_Key_S1_Press();
	}
	else if (key_s1_state == 0x00) {
		On_Key_S1_Release();
	}
}

*/

static void led_7_turn_on_off();

// Start interrupt setting

void Init_Timer0_Interrupt() {
	// start timer0
	TR0 = 1;
	TMOD |= 0x01;
	
	// start timer0 interrupt
	EA = 1;
	ET0 = 1;
	TR0 = 1;
	
	TL0 = 0x18;
	TH0 = 0xFC;
}


void Timer0_Interrupt_Run() interrupt 1 {
	TL0 = 0x18;
	TH0 = 0xFC;

	Button_Key_Check();
	led_7_turn_on_off();
}

sbit LED_7 = P2^7;
static void led_7_turn_on_off() {
	static u16 led_7_count = 0;
	led_7_count++;
	if (led_7_count >= 1000) {
		LED_7 = !LED_7;
		led_7_count = 0;
	}
}

static u16 led_show = 0;

static void Button_Key_OnClick(u8 button_index) {
    led_show = button_index+1;
}

void main() {
    
	u8 dynamic_led_index = 0;
	
	Button_Key_Init(Button_Key_OnClick);

	Init_Timer0_Interrupt();
	Init_HC_74_138();

	led_show = 0;
    
	while(1){
		
		// reflush
		P0 = 0x00; // changing the 138 from 1 to 2, there is some temp state to cause some shadow
		dynamic_led_index = 0;
		Change_HC_74_138(dynamic_led_index+1);
		P0 = static_led[led_show];
	}
  
}
